#!/usr/bin/python3

import sys
import yaml
from jira import JIRA
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart


def read_configuration():
    with open("config.yml", "r") as ymlfile:
        config = yaml.safe_load(ymlfile)
    return config


def list_projects(jira):
    projects = jira.projects()
    return projects


def list_issues(jira, filter=""):
    issues = jira.search_issues(filter)
    return issues


def report_issues(issues):
    report = '''
        <p>Good morning! Your tasks for today:</p>
        <table style="width:100%; border-style:solid; border:1; border-color:#000000"; font-face: arial; font-size:10">\n
        <tr bgcolor="#000000" background="https://ins3cure.com/static/ins3cure_logo.png"><td colspan=5>\n
        <tr bgcolor="#dddddd"><td>Issue ID<td>Status<td>Priority<td>Due date<td>Summary
        '''
    for issue in issues:
        issue_key = issue.key
        priority = str(issue.fields.priority)
        assignee = issue.raw['fields']['assignee']
        if assignee:
            email = assignee['emailAddress']
        else:
            email = "Unassigned"
        status = issue.fields.status.name
        summary = issue.fields.summary[:64]
        duedate = issue.fields.duedate
        if duedate is None:
            duedate = '-'
        report += "<tr><td>{}<td>{}<td>{}<td>{}<td>{}\n".format(issue_key, status, priority, duedate, summary)
    report += '</table>\n<p>Have a nice day!<p>Sincerely, your Jira team.'
    return report

def mail_issues(text):
    smtp_server = config['smtp']['server']
    smtp_port = config['smtp']['port']
    login = config['smtp']['login']
    password = config['smtp']['password']
    message = MIMEMultipart("alternative")
    message["Subject"] = "Your Jira issues"
    message["From"] = config['smtp']['from']
    message["To"] = config['smtp']['to']

    part1 = MIMEText(text, "plain")
    part2 = MIMEText(text, "html")
    message.attach(part1)
    message.attach(part2)

    with smtplib.SMTP_SSL(smtp_server, smtp_port) as server:
        server.login(login, password)
        server.sendmail(config['smtp']['from'], config['smtp']['to'], message.as_string())


if __name__ == '__main__':
    config = read_configuration()
    server = config['jira']['server']
    user = config['jira']['user']
    api_key = config['jira']['api_key']

    options = {
        'server': 'https://' + server
        }
    jira = JIRA(options, basic_auth=(user, api_key))

    # Search for issues
    filter = 'PROJECT != "VULN" AND status != Done'
    issues = list_issues(jira, filter)
    report = report_issues(issues)
    mail_issues(report)

    # Search for vulnerabilities
    filter = 'PROJECT = "VULN" AND status != Done order by priority desc'
    issues = list_issues(jira, filter)
    report = report_issues(issues)
    mail_issues(report)
